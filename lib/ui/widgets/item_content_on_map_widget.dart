import 'package:flutter/material.dart';
import 'package:jongnhams_app/core/helper/constant.dart';
import 'package:jongnhams_app/core/view_models/search_view_model.dart';
import 'package:stacked/stacked.dart';

class ItemContentOnMapWidget extends ViewModelWidget<SearchViewModel> {
  ItemContentOnMapWidget({this.index});
  final int index;
  @override
  Widget build(BuildContext context, SearchViewModel viewModel) {
    var data = viewModel?.lstSearch[index];

    return Container(
      alignment: Alignment.bottomCenter,
      child: AnimatedBuilder(
        animation: viewModel.pageController,
        builder: (BuildContext context, Widget widget) {
          double value = 1;
          if (viewModel.pageController.position.haveDimensions) {
            value = viewModel.pageController.page - index;
            value = (1 - (value.abs() * 0.3) + 0.06).clamp(0.0, 1.0);
          }
          return Center(
            child: SizedBox(
              height: Curves.easeInOut.transform(value) * 120.0,
              width: Curves.easeInOut.transform(value) * 350.0,
              child: widget,
            ),
          );
        },
        child: InkWell(
          onTap: () {
            // moveCamera();
          },
          child: Stack(
            children: [
              Container(
                margin: const EdgeInsets.only(top: 20),
                width: size.width * 0.99,
                decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(10.0),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black54,
                        offset: Offset(0.0, 4.0),
                        blurRadius: 10.0,
                      ),
                    ]),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.white),
                  child: Row(
                    children: [
                      Container(
                        height: 150.0,
                        width: 90.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10.0),
                              topLeft: Radius.circular(10.0)),
                          image: DecorationImage(
                              image: AssetImage("${data?.img ?? ''}"),
                              fit: BoxFit.cover),
                        ),
                      ),
                      SizedBox(width: 5.0),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                width: size.width * 0.4,
                                padding: const EdgeInsets.only(left: 5, top: 5),
                                child: Text(
                                  data?.title ?? '',
                                  style: theme.subtitle2,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              Container(
                                alignment: Alignment.centerRight,
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey),
                                    borderRadius: BorderRadius.circular(4)),
                                margin: const EdgeInsets.only(left: 5, top: 5),
                                padding:
                                    const EdgeInsets.only(left: 5, right: 5),
                                child: Text(
                                  "${data?.distance ?? ''}",
                                  style: theme.subtitle2,
                                ),
                              )
                            ],
                          ),
                          Container(
                            width: 170.0,
                            padding: const EdgeInsets.only(left: 5, top: 5),
                            child: Text(
                              data?.location ?? '',
                              style: theme.bodyText2,
                            ),
                          ),
                          Container(
                            width: size.width * 0.5,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  padding:
                                      const EdgeInsets.only(left: 5, top: 10),
                                  child: Text(
                                    "Open Now",
                                    style: theme.subtitle2
                                        .copyWith(color: Colors.green),
                                  ),
                                ),
                                Container(
                                    padding: const EdgeInsets.only(top: 5),
                                    child: Icon(
                                      Icons.arrow_circle_up_rounded,
                                      color: Colors.greenAccent,
                                    ))
                              ],
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
