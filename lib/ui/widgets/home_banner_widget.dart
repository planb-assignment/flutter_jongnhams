import 'package:flutter/material.dart';
import 'package:jongnhams_app/core/global_function.dart';
import 'package:jongnhams_app/core/helper/colors.dart';
import 'package:jongnhams_app/core/helper/constant.dart';
import 'package:jongnhams_app/core/helper/screen_type.dart';
import 'package:jongnhams_app/ui/views/search_view.dart';

class HomeBannerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      child: Stack(
        children: [
          Image.asset(
            "lib/assets/images/resturant.jpg",
            fit: BoxFit.cover,
            width: size.width,
            height: 200,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 1),
            child: Center(
              child: Container(
                child: Image.asset(
                  "lib/assets/images/logoslide.png",
                  height: 60,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 120),
            child: Center(
              child: Container(
                width: size.width * 0.9,
                height: 50,
                child: TextFormField(
                  onTap: () {
                    startNewScreen(ScreenType.SCREEN_WIDGET, SearchView());
                  },
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.search,
                      size: 25,
                      color: PRIMARY_COLOR,
                    ),
                    filled: true,
                    contentPadding: const EdgeInsets.only(top: 5),
                    hintStyle: theme.bodyText1.copyWith(color: Colors.black54),
                    hintText: "Search Restaurant",
                    fillColor: Colors.white,
                    focusColor: Colors.white,
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
