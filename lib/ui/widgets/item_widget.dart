import 'package:flutter/material.dart';
import 'package:jongnhams_app/core/helper/constant.dart';

class ItemWidget extends StatelessWidget {
  ItemWidget({this.bgImage, this.title, this.icon});
  final String bgImage;
  final String title;
  final String icon;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
      padding: const EdgeInsets.only(left: 5, top: 5, right: 5),
      child: Stack(
        children: [
          Container(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: Image.network(
                "$bgImage",
                height: 180,
                width: 130,
                color: Colors.black,
                colorBlendMode: BlendMode.softLight,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 50),
            child: Container(
              width: 130,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Center(
                    child: Container(
                      child: Image.network(
                        "$icon",
                        height: 40,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    alignment: Alignment.center,
                    child: Text(
                      "$title",
                      style: theme.subtitle2.copyWith(color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
