import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jongnhams_app/core/view_models/search_view_model.dart';
import 'package:stacked/stacked.dart';

import 'item_content_on_map_widget.dart';

class GoogleMapWidget extends ViewModelWidget<SearchViewModel> {
  @override
  Widget build(BuildContext context, SearchViewModel viewModel) {
    return Stack(
      children: [
        GoogleMap(
          mapType: MapType.normal,
          markers: viewModel.allMarkers,
          myLocationEnabled: true,
          myLocationButtonEnabled: true,
          initialCameraPosition: viewModel.kGooglePlex,
          onMapCreated: (GoogleMapController controller) {
            viewModel.onMapCreated(controller);
          },
        ),
        Positioned(
          bottom: 20.0,
          child: Container(
            height: 150.0,
            width: MediaQuery.of(context).size.width,
            child: PageView.builder(
              controller: viewModel.pageController,
              itemCount: viewModel?.lstSearch?.length ?? 0,
              itemBuilder: (BuildContext context, int index) {
                return ItemContentOnMapWidget(
                  index: index,
                );
              },
            ),
          ),
        )
      ],
    );
  }
}
