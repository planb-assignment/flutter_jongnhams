import 'package:flutter/material.dart';
import 'package:jongnhams_app/core/helper/colors.dart';
import 'package:stacked/stacked.dart';

class ViewModelStateHandlerWidget<T extends BaseViewModel>
    extends ViewModelWidget<T> {
  ViewModelStateHandlerWidget({
    @required this.child,
    this.retryFunction,
  });

  final Widget child;
  final Function retryFunction;

  @override
  Widget build(BuildContext context, T model) {
    if (!model.initialised) {
      return Material(
        child: Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(PRIMARY_COLOR),
          ),
        ),
      );
    }

    if (model.hasError) {
      return Scaffold(
        body: Material(
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("${model.modelError}"),
                  ),
                  this.retryFunction != null
                      ? FlatButton(
                          onPressed: () {
                            this.retryFunction();
                          },
                          child: Text("RETRY"),
                          textColor: PRIMARY_COLOR,
                        )
                      : Container()
                ],
              ),
            ),
          ),
        ),
      );
    }

    return Stack(
      fit: StackFit.expand,
      children: [
        this.child,
        (model?.isBusy ?? false)
            ? Container(
                color: Colors.black45,
                child: Center(
                  child: Container(
                    padding: const EdgeInsets.all(16),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white,
                    ),
                    child: Material(
                      color: Colors.transparent,
                      elevation: 0,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            "Loading...",
                            style: Theme.of(context)
                                .textTheme
                                .overline
                                .copyWith(color: Colors.black, fontSize: 12),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(PRIMARY_COLOR),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            : Container(),
      ],
    );
  }
}
