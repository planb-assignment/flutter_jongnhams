import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:jongnhams_app/core/helper/constant.dart';
import 'package:jongnhams_app/core/view_models/search_view_model.dart';
import 'package:stacked/stacked.dart';

class SearchItemWidget extends ViewModelWidget<SearchViewModel> {
  SearchItemWidget(this.index);

  final int index;
  @override
  Widget build(BuildContext context, SearchViewModel model) {
    var data = model?.lstSearch[index];
    return Container(
      padding: const EdgeInsets.only(left: 5, top: 15, right: 5),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: Image.asset(
              "${data.img}",
              height: 110,
              width: 110,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 2, left: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: size.width * 0.5,
                        child: Text(
                          "${data?.title ?? ''}",
                          maxLines: 1,
                          style: theme.subtitle2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Card(
                          child: Container(
                              padding: const EdgeInsets.all(2),
                              alignment: Alignment.center,
                              child: Text("${data?.distance ?? ''} km")))
                    ],
                  ),
                ),
                Row(
                  children: [
                    RatingBar.builder(
                      initialRating: data.star,
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemSize: 20,
                      itemCount: 5,
                      itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {},
                    ),
                    Text(" ${data.star} | "),
                    Icon(Icons.remove_red_eye),
                    SizedBox(
                      width: 5,
                    ),
                    Text("${data.views.toInt()} Views")
                  ],
                ),
                Row(
                  children: [
                    Text(
                        "${data.rateAmount.toInt()} ${data.rateAmount <= 0 ? 'Rating' : 'Ratings'} | "),
                    Text(
                        "${data.rateAmount.toInt()}  ${data.rateAmount <= 0 ? 'Review' : 'Reviews'}"),
                  ],
                ),
                Row(
                  children: [
                    Icon(Icons.fastfood),
                    Text("${data.category} | "),
                    Icon(Icons.location_on),
                    Container(
                        width: 80,
                        child: Text(
                          "${data.location}",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        )),
                    TextButton(onPressed: () {}, child: Text("Open Now"))
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
