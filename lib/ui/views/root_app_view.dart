import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:jongnhams_app/core/helper/colors.dart';
import 'package:jongnhams_app/core/view_models/root_app_view_model.dart';
import 'package:stacked/stacked.dart';

class RootAppView extends ViewModelBuilderWidget<RootAppViewModel> {
  @override
  viewModelBuilder(BuildContext context) {
    return RootAppViewModel();
  }

  @override
  void onViewModelReady(RootAppViewModel viewModel) {
    viewModel.getInstance();
  }

  @override
  Widget builder(BuildContext context, model, Widget child) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0),
        child: Container(
          decoration: BoxDecoration(color: PRIMARY_COLOR),
        ),
      ),
      body: PageView(
        controller: model.pageController,
        children: model.lstMainScreen,
        physics: NeverScrollableScrollPhysics(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedItemColor: PRIMARY_COLOR,
        unselectedItemColor: Colors.grey.withOpacity(0.9),
        onTap: model.onBottomNavigationItemTapped,
        currentIndex: model.currentBottomNavIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home_outlined),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: Icon(MaterialCommunityIcons.gift),
            label: "Coupon",
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.favorite), label: "Favorite"),
          BottomNavigationBarItem(
              icon: Icon(Ionicons.person_circle_sharp), label: "Me"),
        ],
      ),
    );
  }
}
