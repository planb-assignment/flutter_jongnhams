import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:jongnhams_app/core/helper/colors.dart';
import 'package:jongnhams_app/core/helper/constant.dart';
import 'package:jongnhams_app/core/view_models/search_view_model.dart';
import 'package:jongnhams_app/ui/widgets/google_map_widget.dart';
import 'package:jongnhams_app/ui/widgets/search_item_wdiget.dart';
import 'package:stacked/stacked.dart';

class SearchView extends ViewModelBuilderWidget<SearchViewModel> {
  @override
  SearchViewModel viewModelBuilder(BuildContext context) {
    return SearchViewModel();
  }

  @override
  void onViewModelReady(SearchViewModel viewModel) {
    viewModel.getInstance();
  }

  @override
  Widget builder(
      BuildContext context, SearchViewModel viewModel, Widget child) {
    return Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(0.0),
            child: Container(decoration: BoxDecoration(color: PRIMARY_COLOR))),
        body: Column(
          children: [
            Container(
              color: PRIMARY_COLOR,
              padding: const EdgeInsets.only(
                  left: 10, right: 10, top: 15, bottom: 15),
              child: Row(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      size: 35,
                      color: Colors.white,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: 50,
                      child: TextFormField(
                        onChanged: (value) {
                          viewModel.searchData(value);
                        },
                        decoration: InputDecoration(
                          prefixIcon: Icon(
                            Icons.search,
                            size: 25,
                            color: PRIMARY_COLOR,
                          ),
                          filled: true,
                          contentPadding: const EdgeInsets.only(top: 5),
                          hintStyle:
                              theme.bodyText1.copyWith(color: Colors.black54),
                          hintText: "Search Restaurant",
                          fillColor: Colors.white,
                          focusColor: Colors.white,
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  viewModel.isSearch
                      ? GestureDetector(
                          onTap: () {
                            viewModel.setIsSearch(false);
                          },
                          child: Icon(
                            Fontisto.earth,
                            size: 35,
                            color: Colors.white,
                          ),
                        )
                      : GestureDetector(
                          onTap: () {
                            viewModel.setIsSearch(true);
                          },
                          child: Icon(
                            Icons.menu,
                            color: Colors.white,
                            size: 35,
                          ),
                        )
                ],
              ),
            ),
            Container(
              color: Colors.black87,
              padding: const EdgeInsets.only(left: 5, right: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children:
                    List.generate(viewModel?.menuButton?.length ?? 0, (index) {
                  return Expanded(
                    child: index != 0
                        ? Container(
                            margin: const EdgeInsets.only(left: 5),
                            child: ElevatedButton(
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  Colors.grey.shade700,
                                ),
                              ),
                              onPressed: () {},
                              child:
                                  Text("${viewModel?.menuButton[index] ?? ''}"),
                            ),
                          )
                        : Container(
                            margin: const EdgeInsets.only(left: 5),
                            child: ElevatedButton.icon(
                              icon: Icon(Icons.filter_alt_outlined),
                              onPressed: () {},
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                    Colors.grey.shade700),
                              ),
                              label: Text("${viewModel.menuButton[index]}"),
                            ),
                          ),
                  );
                }),
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.only(top: 5, left: 5),
              child: Text(
                "227 restaurants found",
                style: theme.subtitle1,
              ),
            ),
            Expanded(
                child: viewModel.isSearch
                    ? ListView.builder(
                        shrinkWrap: true,
                        physics: BouncingScrollPhysics(),
                        itemCount: viewModel?.lstSearch?.length ?? 0,
                        itemBuilder: (context, index) {
                          return SearchItemWidget(index);
                        },
                      )
                    : GoogleMapWidget())
          ],
        ));
  }
}
