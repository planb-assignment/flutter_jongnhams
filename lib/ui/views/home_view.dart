import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:jongnhams_app/core/helper/colors.dart';
import 'package:jongnhams_app/core/helper/constant.dart';
import 'package:jongnhams_app/core/view_models/home_view_model.dart';
import 'package:jongnhams_app/ui/widgets/home_banner_widget.dart';
import 'package:jongnhams_app/ui/widgets/item_widget.dart';
import 'package:jongnhams_app/ui/widgets/view_model_state_handler_widget.dart';
import 'package:stacked/stacked.dart';

class HomeView extends ViewModelBuilderWidget<HomeViewModel> {
  @override
  void onViewModelReady(HomeViewModel viewModel) {
    viewModel.getInstance();
  }

  @override
  HomeViewModel viewModelBuilder(BuildContext context) {
    return HomeViewModel();
  }

  @override
  Widget builder(BuildContext context, HomeViewModel viewModel, Widget child) {
    return ViewModelStateHandlerWidget<HomeViewModel>(
        retryFunction: viewModel.getInstance,
        child: Scaffold(
            backgroundColor: Colors.white,
            body: SingleChildScrollView(
              child: Column(
                children: [
                  HomeBannerWidget(),
                  Material(
                    elevation: 2,
                    color: Colors.white,
                    child: Container(
                      width: size.width,
                      alignment: Alignment.center,
                      height: 80,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  right: BorderSide(color: Colors.black12),
                                ),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.my_location,
                                    color: PRIMARY_COLOR,
                                  ),
                                  Text("Near me"),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  right: BorderSide(
                                    color: Colors.black12,
                                  ),
                                ),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    MaterialCommunityIcons.food_fork_drink,
                                    color: PRIMARY_COLOR,
                                  ),
                                  Text("Type of food"),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.location_on,
                                    color: PRIMARY_COLOR,
                                  ),
                                  Text("Location"),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: const EdgeInsets.only(top: 20, left: 5),
                    child: Text(
                      "Recommended",
                      style: theme.subtitle1,
                    ),
                  ),
                  Container(
                    width: size.width,
                    child: Row(
                      children: [
                        Expanded(
                          child: ItemWidget(
                            icon:
                                "https://www.jongnhams.com/uploads/1/2018-06/scissors.png",
                            bgImage:
                                "https://www.jongnhams.com/uploads/1/2018-06/shutterstock_91092917.png",
                            title: "Most Used Coupon",
                          ),
                        ),
                        Expanded(
                          child: ItemWidget(
                            icon:
                                "https://www.jongnhams.com/uploads/1/2018-06/rating_svgrepo_com.png",
                            bgImage:
                                "https://www.jongnhams.com/uploads/92635-JN-10-26.jpg",
                            title: "Most View",
                          ),
                        ),
                        Expanded(
                          child: ItemWidget(
                            icon:
                                "https://www.jongnhams.com/uploads/1/2018-06/review.png",
                            bgImage:
                                "https://www.jongnhams.com/uploads/1/2018-06/shutterstock_81319588.png",
                            title: "Top Review",
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: const EdgeInsets.only(top: 15, left: 5, bottom: 5),
                    child: Text(
                      "Coupon",
                      style: theme.subtitle1,
                    ),
                  ),
                  Container(
                    height: 180,
                    width: size.width,
                    child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: viewModel.images.length,
                        itemBuilder: (context, index) {
                          var data = viewModel.images[index];
                          return Container(
                            height: 180,
                            padding: const EdgeInsets.only(left: 5),
                            width: size.width * 0.8,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(5),
                              child: Image.asset(
                                "$data",
                                fit: BoxFit.cover,
                              ),
                            ),
                          );
                        }),
                  )
                ],
              ),
            )));
  }
}
