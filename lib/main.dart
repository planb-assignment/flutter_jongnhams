import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jongnhams_app/ui/views/splash_view.dart';

import 'core/helper/colors.dart';
import 'core/services/connection_internet_service.dart';
import 'core/services/global_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  runApp(
    EasyLocalization(
        supportedLocales: [
          Locale('en', 'US'),
          Locale('km', 'KH'),
        ],
        path: 'lib/assets/languages', // <-- change patch to your
        fallbackLocale: Locale('en', 'US'),
        child: MyApp()),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final TextTheme textTheme = Theme.of(context).textTheme;
    ConnectionInternetService().initialize();

    return MaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      debugShowCheckedModeBanner: false,
      theme: ThemeData.light().copyWith(
          textTheme: GoogleFonts.robotoTextTheme(textTheme).copyWith(
            bodyText1: GoogleFonts.roboto(textStyle: textTheme.bodyText1),
            bodyText2: GoogleFonts.roboto(textStyle: textTheme.bodyText2),
            subtitle1: GoogleFonts.roboto(
                textStyle: textTheme.subtitle1
                    .copyWith(fontSize: 18, fontWeight: FontWeight.bold)),
            subtitle2: GoogleFonts.roboto(textStyle: textTheme.subtitle2),
            headline5: GoogleFonts.roboto(textStyle: textTheme.headline5),
            headline6: GoogleFonts.roboto(textStyle: textTheme.headline6),
            caption: GoogleFonts.roboto(textStyle: textTheme.caption),
          ),
          appBarTheme: AppBarTheme(
            color: PRIMARY_COLOR,
          ),
          indicatorColor: PRIMARY_COLOR),
      home: SplashView(),
      navigatorKey: GlobalService().navigator,
    );
  }
}
