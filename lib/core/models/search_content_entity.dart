import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jongnhams_app/generated/json/base/json_convert_content.dart';
import 'package:jongnhams_app/generated/json/base/json_field.dart';

class SearchContentEntity with JsonConvert<SearchContentEntity> {
  String img;
  @JSONField(name: "rate_amount")
  double rateAmount;
  String title;
  String category;
  String location;
  double distance;
  double views;
  double star;
  LatLng locationCoords;

  SearchContentEntity(
      {this.img,
      this.rateAmount,
      this.title,
      this.category,
      this.location,
      this.distance,
      this.views,
      this.locationCoords,
      this.star});
}
