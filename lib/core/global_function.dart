import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:jongnhams_app/core/services/global_service.dart';
import 'package:logger/logger.dart';

import 'helper/screen_type.dart';

///Handle log printing in debug build
printDebugMessage(data) {
  if (!kReleaseMode) {
    if (data.toString().length < 2000) {
      Logger().d(data);
    }
  }
}

startNewScreen(ScreenType screenType, Widget widget) {
  if (screenType == ScreenType.SCREEN_WIDGET_REPLACEMENT) {
    GlobalService().navigator.currentState.pushReplacement(
          MaterialPageRoute(
            builder: (context) => widget,
          ),
        );
  } else {
    GlobalService().navigator.currentState.push(
          MaterialPageRoute(
            builder: (context) => widget,
          ),
        );
  }
}
