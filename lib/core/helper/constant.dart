import 'package:flutter/material.dart';
import 'package:jongnhams_app/core/services/global_service.dart';

final size = MediaQuery.of(GlobalService().context).size;
final theme = Theme.of(GlobalService().context).textTheme;
