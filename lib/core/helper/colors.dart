import 'dart:ui';

import 'package:flutter/material.dart';

const Color PRIMARY_COLOR = Color.fromRGBO(189, 22, 34, 1);
const Color SECONDARY_COLOR = Color.fromRGBO(252, 236, 35, 1);
