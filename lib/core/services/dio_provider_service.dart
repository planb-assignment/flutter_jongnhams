import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:jongnhams_app/core/helper/config.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class DioProviderService {
  DioProviderService._apiConstructor();

  static final DioProviderService _instance =
      DioProviderService._apiConstructor();

  factory DioProviderService() {
    return _instance;
  }

  Dio _dio = Dio(
    BaseOptions(
      connectTimeout: 5000,
      sendTimeout: 5000,
      receiveTimeout: 5000,
      baseUrl: baseApiUrl,
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
    ),
  );

  Dio get getDioInstance => _dio;

  getInstance() {
    if (!kReleaseMode) {
      _dio.interceptors.add(
        PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: false,
          error: true,
          compact: true,
          maxWidth: 90,
        ),
      );
    }
  }

  setUserAccessToken(String accessToken) {
    _dio.options.headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer $accessToken",
      "Accept": "application/json",
    };
  }

  clearUserAccessToken() {
    _dio.options.headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
    };
  }
}
