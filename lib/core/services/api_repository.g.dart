// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_repository.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _ApiRepository implements ApiRepository {
  _ApiRepository(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    baseUrl ??= 'https://www.netfreemovie.com/';
  }

  final Dio _dio;

  String baseUrl;
}
