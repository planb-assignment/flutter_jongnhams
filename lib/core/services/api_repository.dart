import 'package:dio/dio.dart';
import 'package:jongnhams_app/core/helper/config.dart';
import 'package:retrofit/retrofit.dart';

part 'api_repository.g.dart';

@RestApi(baseUrl: baseApiUrl)
abstract class ApiRepository {
  factory ApiRepository(Dio dio, {String baseUrl}) = _ApiRepository;
}
