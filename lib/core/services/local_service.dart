import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive/hive.dart';
import 'package:jongnhams_app/core/enums/local_service.dart';
import 'package:path_provider/path_provider.dart';

class LocalService {
  LocalService._apiConstructor();

  static final LocalService _instance = LocalService._apiConstructor();

  factory LocalService() {
    return _instance;
  }

  Box _localObjectBox;

  getInstance() async {
    final directory = await getApplicationDocumentsDirectory();
    final storage = FlutterSecureStorage();

    String encryptKey = await storage.read(key: "key");

    if (encryptKey == null) {
      var key1 = Hive.generateSecureKey();
      await storage.write(key: "key", value: base64.encode(key1));
      encryptKey = await storage.read(key: "key");
    }

    List<int> key = base64.decode(encryptKey);

    Hive..init(directory.path);
    _localObjectBox = await Hive.openBox('APP_DATA', encryptionKey: key);
  }

  saveValue(LocalDataFieldName localDataFieldName, dynamic value) async {
    if (_localObjectBox == null) {
      await getInstance();
    }

    _localObjectBox.put(localDataFieldName.toString(), value);
  }

  dynamic getSavedValue(LocalDataFieldName localDataFieldName) async {
    if (_localObjectBox == null) {
      await getInstance();
    }

    return _localObjectBox.get(localDataFieldName.toString());
  }

  deleteSavedValue(LocalDataFieldName localDataFieldName) async {
    if (_localObjectBox == null) {
      await getInstance();
    }

    _localObjectBox.delete(localDataFieldName.toString());
  }

  deleteAllSavedValue() async {
    if (_localObjectBox == null) {
      await getInstance();
    }
    _localObjectBox.clear();
  }
}
