import 'package:flutter/material.dart';
import 'package:jongnhams_app/core/global_function.dart';
import 'package:jongnhams_app/core/helper/screen_type.dart';
import 'package:jongnhams_app/core/services/connection_internet_service.dart';
import 'package:jongnhams_app/core/services/dio_provider_service.dart';
import 'package:jongnhams_app/core/services/global_service.dart';
import 'package:jongnhams_app/core/services/local_service.dart';
import 'package:jongnhams_app/ui/views/root_app_view.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:stacked/stacked.dart';

class SplashViewModel extends BaseViewModel {
  getInstance() async {
    DioProviderService().getInstance();
    await LocalService().getInstance();
    requestPermission();
    final snackBar = SnackBar(
      content: Text('No internet connection'),
      duration: Duration(minutes: 1),
      action: SnackBarAction(
        label: 'Please try!',
        onPressed: () {
          getInstance();
        },
      ),
    );

    if (await ConnectionInternetService().checkConnection() != true) {
      ScaffoldMessenger.of(GlobalService().context).showSnackBar(snackBar);
    }
    Future.delayed(
      const Duration(seconds: 4),
      () async {
        if (await ConnectionInternetService().checkConnection()) {
          startNewScreen(ScreenType.SCREEN_WIDGET_REPLACEMENT, RootAppView());
        }
      },
    );
  }

  requestPermission() async {
    if (await Permission.location.request().isGranted) {
      Map<Permission, PermissionStatus> statuses = await [
        Permission.location,
      ].request();
    }
  }
}
