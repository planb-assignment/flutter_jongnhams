import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jongnhams_app/core/models/search_content_entity.dart';
import 'package:stacked/stacked.dart';

class SearchViewModel extends BaseViewModel {
  List<String> menuButton = ["Filter", "Ranking", "View", "Price"];
  List<SearchContentEntity> lstSearch = [
    SearchContentEntity(
        img: "lib/assets/images/aws.jpg",
        title: "Café Amazon - In PTT",
        rateAmount: 20,
        star: 5,
        distance: 0.8,
        location: "Phnom Penh",
        views: 400,
        category: "Coffee",
        locationCoords: LatLng(11.5480498, 104.8807732)),
    SearchContentEntity(
        img: "lib/assets/images/arabitia.jpg",
        title: "Arabitia Coffee",
        rateAmount: 20,
        star: 5,
        distance: 0.8,
        location: "Kampong Cham",
        views: 400,
        category: "Coffee",
        locationCoords: LatLng(11.558304, 104.8881399)),
    SearchContentEntity(
        img: "lib/assets/images/true.jpg",
        title: "True Coffee ",
        rateAmount: 20,
        star: 4,
        distance: 0.8,
        location: "Phnom Penh",
        views: 200,
        category: "Coffee",
        locationCoords: LatLng(11.5474264, 104.8936446)),
    SearchContentEntity(
        img: "lib/assets/images/coffee1979.jpg",
        title: "Coffee 1979",
        rateAmount: 20,
        star: 3,
        distance: 0.8,
        location: "Phnom Penh",
        views: 100,
        category: "Coffee",
        locationCoords: LatLng(11.570718, 104.8691398)),
    SearchContentEntity(
        img: "lib/assets/images/tpr.jpg",
        title: "TPR Coffee",
        rateAmount: 60,
        star: 5,
        distance: 0.8,
        location: "Phnom Penh City",
        views: 50,
        category: "Coffee",
        locationCoords: LatLng(
          11.5408112,
          104.8805845,
        )),
    SearchContentEntity(
        img: "lib/assets/images/prenuim.jpg",
        title: "Premium Coffee",
        rateAmount: 90,
        star: 5,
        distance: 0.5,
        location: "Teuk Tla・Rupp",
        views: 50,
        category: "Coffee",
        locationCoords: LatLng(11.5854736, 104.8814846)),
  ];
  TextEditingController txtSearch = TextEditingController();

  GoogleMapController mapController;

  Set<Marker> allMarkers = {};

  bool isSearch = true;
  PageController pageController;
  BitmapDescriptor pinIcon;
  int prevPage;
  final CameraPosition kGooglePlex = CameraPosition(
    target: LatLng(11.5793304, 104.7497507),
    zoom: 18,
  );
  getInstance() {
    clearErrors();
    setBusy(false);
    notifyListeners();

    setCustomMapPin();
    pageController = PageController(initialPage: 1, viewportFraction: 0.8)
      ..addListener(onScroll);

    setInitialised(true);
    notifyListeners();
  }

  setIsSearch(bool value) {
    isSearch = value;
    notifyListeners();
  }

  searchData(String value) {
    print("value=> $value");
    var data =
        lstSearch.where((element) => (element.title.contains(value))).toList();
    print("dataleng=> ${data.length}");
    if (value.isEmpty) {
      print("meme");
      lstSearch.clear();
      notifyListeners();
    }
    if (data.length > 0) {
      lstSearch = data;
    }
    notifyListeners();
  }

  void setCustomMapPin() async {
    getBytesFromAsset('lib/assets/images/pinLocation.png', 64).then((onValue) {
      print("value=> $onValue");
      pinIcon = BitmapDescriptor.fromBytes(onValue);
    });
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  onScroll() {
    if (pageController.page.toInt() != prevPage) {
      prevPage = pageController.page.toInt();
      moveCamera();
    }
  }

  void onMapCreated(GoogleMapController controller) {
    mapController = controller;

    lstSearch.forEach((element) {
      allMarkers.add(Marker(
          markerId: MarkerId(element.title),
          icon: pinIcon,
          infoWindow: InfoWindow(title: element.title, snippet: element.title),
          position: element.locationCoords));
    });
    notifyListeners();
  }

  moveCamera() {
    mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: lstSearch[pageController.page.toInt()].locationCoords,
          zoom: 18,
          bearing: 45.0,
          tilt: 45.0,
        ),
      ),
    );
  }
}
