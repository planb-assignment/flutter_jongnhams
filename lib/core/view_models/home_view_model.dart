import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:stacked/stacked.dart';

class HomeViewModel extends BaseViewModel {
  List<String> images = [
    "lib/assets/images/banner1.png",
    "lib/assets/images/banner2.jpg",
    "lib/assets/images/banner3.jpg",
  ];
  List<String> menuButton = ["Filter", "Rankin", "View", "Price"];

  bool isSearch = false;

  setIsSearch(bool value) {
    isSearch = value;
    notifyListeners();
  }

  RefreshController refreshController = RefreshController();

  getInstance() {
    clearErrors();
    setBusy(false);
    notifyListeners();

    refreshController.refreshCompleted();
    setInitialised(true);
    notifyListeners();
  }
}
