import 'package:flutter/material.dart';
import 'package:jongnhams_app/core/services/connection_internet_service.dart';
import 'package:jongnhams_app/core/services/global_service.dart';
import 'package:jongnhams_app/ui/views/coupon_view.dart';
import 'package:jongnhams_app/ui/views/favorite_view.dart';
import 'package:jongnhams_app/ui/views/home_view.dart';
import 'package:jongnhams_app/ui/views/profile_view.dart';
import 'package:stacked/stacked.dart';

class RootAppViewModel extends BaseViewModel {
  final List<Widget> lstMainScreen = [
    HomeView(),
    CouponView(),
    FavoriteView(),
    ProfileView()
  ];
  final PageController pageController =
      PageController(keepPage: true, initialPage: 0);

  int currentBottomNavIndex = 0;

  onBottomNavigationItemTapped(int index) {
    pageController.jumpToPage(index);
    currentBottomNavIndex = index;
    notifyListeners();
  }

  getInstance() async {
    print("data");
    clearErrors();
    setBusy(true);
    notifyListeners();
    final snackBar = SnackBar(
      content: Text('No internet connection'),
      duration: Duration(minutes: 1),
      action: SnackBarAction(
        label: 'Please try!',
        onPressed: () {
          getInstance();
        },
      ),
    );

    if (await ConnectionInternetService().checkConnection() != true) {
      ScaffoldMessenger.of(GlobalService().context).showSnackBar(snackBar);
    }
    ConnectionInternetService().connectionChange.listen((canConnect) {
      print("can connect=> $canConnect");

      if (!canConnect ?? false) {
        ScaffoldMessenger.of(GlobalService().context).showSnackBar(snackBar);
      }
    });
    setInitialised(true);
    notifyListeners();
  }
}
