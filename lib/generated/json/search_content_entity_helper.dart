import 'package:jongnhams_app/core/models/search_content_entity.dart';

searchContentEntityFromJson(
    SearchContentEntity data, Map<String, dynamic> json) {
  if (json['img'] != null) {
    data.img = json['img'].toString();
  }
  if (json['rate_amount'] != null) {
    data.rateAmount = json['rate_amount'] is String
        ? double.tryParse(json['rate_amount'])
        : json['rate_amount'].toDouble();
  }
  if (json['title'] != null) {
    data.title = json['title'].toString();
  }
  if (json['category'] != null) {
    data.category = json['category'].toString();
  }
  if (json['location'] != null) {
    data.location = json['location'].toString();
  }
  if (json['distance'] != null) {
    data.distance = json['distance'] is String
        ? double.tryParse(json['distance'])
        : json['distance'].toDouble();
  }
  if (json['views'] != null) {
    data.views = json['views'] is String
        ? double.tryParse(json['views'])
        : json['views'].toDouble();
  }
  if (json['star'] != null) {
    data.star = json['star'] is String
        ? double.tryParse(json['star'])
        : json['star'].toDouble();
  }

  return data;
}

Map<String, dynamic> searchContentEntityToJson(SearchContentEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['img'] = entity.img;
  data['rate_amount'] = entity.rateAmount;
  data['title'] = entity.title;
  data['category'] = entity.category;
  data['location'] = entity.location;
  data['distance'] = entity.distance;
  data['views'] = entity.views;
  data['star'] = entity.star;
  data['locationCoords'] = entity.locationCoords?.toJson();
  return data;
}
